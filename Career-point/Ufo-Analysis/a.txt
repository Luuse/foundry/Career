<?xml version="1.0" encoding="UTF-8"?>
<glyph name="a" format="1">
  <advance width="513"/>
  <unicode hex="0061"/>
  <outline>
    <contour>
      <point x="173" y="363" type="line"/>
      <point x="173" y="400"/>
      <point x="24" y="309" type="curve" smooth="yes"/>
      <point x="24" y="311" type="line" smooth="yes"/>
      <point x="24" y="334"/>
      <point x="37" y="361"/>
      <point x="63" y="393" type="curve" smooth="yes"/>
      <point x="108" y="446"/>
      <point x="201" y="510"/>
      <point x="300" y="510" type="curve" smooth="yes"/>
      <point x="371" y="510"/>
      <point x="415" y="482"/>
      <point x="433" y="430" type="curve" smooth="yes"/>
      <point x="437" y="416"/>
      <point x="440" y="402"/>
      <point x="440" y="387" type="curve" smooth="yes"/>
      <point x="440" y="62" type="line" smooth="yes"/>
      <point x="440" y="46"/>
      <point x="449" y="37"/>
      <point x="463" y="33" type="curve" smooth="yes"/>
      <point x="464" y="33"/>
      <point x="466" y="32"/>
      <point x="467" y="32" type="curve" smooth="yes"/>
      <point x="475" y="32"/>
      <point x="485" y="37"/>
      <point x="497" y="45" type="curve"/>
      <point x="504" y="40" type="line"/>
      <point x="488" y="19"/>
      <point x="444" y="-10"/>
      <point x="404" y="-10" type="curve" smooth="yes"/>
      <point x="347" y="-10"/>
      <point x="308" y="16"/>
      <point x="306" y="66" type="curve"/>
      <point x="273" y="20"/>
      <point x="200" y="-10"/>
      <point x="142" y="-10" type="curve" smooth="yes"/>
      <point x="82" y="-10"/>
      <point x="26" y="32"/>
      <point x="26" y="91" type="curve" smooth="yes"/>
      <point x="26" y="94" type="line" smooth="yes"/>
      <point x="26" y="176"/>
      <point x="118" y="213"/>
      <point x="176" y="240" type="curve" smooth="yes"/>
      <point x="213" y="257"/>
      <point x="263" y="275"/>
      <point x="306" y="292" type="curve"/>
      <point x="306" y="430" type="line" smooth="yes"/>
      <point x="306" y="463"/>
      <point x="301" y="488"/>
      <point x="267" y="489" type="curve"/>
      <point x="212" y="489"/>
      <point x="172" y="465"/>
      <point x="172" y="410" type="curve"/>
    </contour>
    <contour>
      <point x="306" y="92" type="curve"/>
      <point x="306" y="274" type="line"/>
      <point x="228" y="249"/>
      <point x="164" y="224"/>
      <point x="164" y="132" type="curve" smooth="yes"/>
      <point x="164" y="125" type="line" smooth="yes"/>
      <point x="165" y="81"/>
      <point x="173" y="35"/>
      <point x="227" y="35" type="curve" smooth="yes"/>
      <point x="253" y="35"/>
      <point x="289" y="59"/>
    </contour>
  </outline>
  <lib>
    <dict>
      <key>com.fontlab.hintData</key>
      <dict>
	<key>hhints</key>
	<array>
	  <dict>
	    <key>position</key>	    <integer>-10</integer>
	    <key>width</key>	    <integer>45</integer>
	  </dict>
	  <dict>
	    <key>position</key>	    <integer>274</integer>
	    <key>width</key>	    <integer>18</integer>
	  </dict>
	  <dict>
	    <key>position</key>	    <integer>489</integer>
	    <key>width</key>	    <integer>21</integer>
	  </dict>
	</array>
	<key>vhints</key>
	<array>
	  <dict>
	    <key>position</key>
	    <integer>24</integer>
	    <key>width</key>
	    <integer>149</integer>
	  </dict>
	  <dict>
	    <key>position</key>
	    <integer>26</integer>
	    <key>width</key>
	    <integer>138</integer>
	  </dict>
	  <dict>
	    <key>position</key>
	    <integer>306</integer>
	    <key>width</key>
	    <integer>134</integer>
	  </dict>
	</array>
      </dict>
    </dict>
  </lib>
</glyph>
