#! /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys

fontName = sys.argv[1]
#FontFile = sys.argv[2]

font = fontforge.open('Career_empty.sfd')
svg_letter = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
svg_number = {'one', 'two', 'three','four','five', 'six', 'seven','height', 'nine', 'zero'}
svg_special = {'ampersand','asciicircum', 'asterisk', 'colon', 'equal', 'exclam', 'hyphen', 'parenleft', 'parenright', 'percent', 'period', 'plus', 'question', 'quotedbl','quotesingle', 'semicolon', 'slash', 'underscore'}


for letter_svg_final in svg_number:

	#letter = letter_svg_final.split("/")[-1].replace(".svg", "")
	print(letter_svg_final)
	glyph = font.createMappedChar(letter_svg_final)
	glyph.importOutlines('svg/'+letter_svg_final+'.svg')

font.generate(fontName+'.otf')
#font.generate(fontName+'.ttf')
