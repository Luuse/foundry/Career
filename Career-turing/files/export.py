#! /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import sys

font = sys.argv[2]
file_format = sys.argv[1]

for gly in fontforge.open(font).glyphs():
	gly.export(file_format + "/" +gly.glyphname + "." + file_format)

	print ('svg/'+gly.glyphname)

