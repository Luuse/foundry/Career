#! /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import sys

font = sys.argv[2]
file_format = sys.argv[1]

for gly in fontforge.open(font).glyphs():
	gly.export("png/" +gly.glyphname + ".png", 1000)
	print(" ")
	print ('                                                           |'+gly.glyphname+'  .png export ✔ |')
