#! /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys

fontName = sys.argv[1]

font = fontforge.open('files_final/'+fontName+'_Skeleton.otf')


for glyph in font.glyphs():
    #glyph.stroke('circular',  100, 'butt', 'bevel', ('cleanup',))
    #glyph.stroke('caligraphic',  120, 10, 75, ('removeexternal'))
    glyph.stroke('eliptical',  140, 2, 10, 'butt','miter',('cleanup'))



font.generate('files_final/'+fontName+'.otf')
font.generate('files_final/ttf/'+fontName+'.ttf')
