#! /usr/bin/env python
# -*- coding: utf-8 -*-

import fontforge
import glob
import sys

fontName = sys.argv[1]
#FontFile = sys.argv[2]


print "skeleton mode"

font = fontforge.open('import/Career_empty.sfd')
svg_final = glob.glob('svg_skeleton/*.svg')


for letter_svg_final in svg_final:

	letter = letter_svg_final.split("/")[-1].replace(".svg", "")
	print(letter)
	glyph = font.createMappedChar(letter)
	glyph.importOutlines(letter_svg_final)
	glyph.correctDirection()

font.generate('files_final/'+fontName+'_Skeleton.otf')
font.generate('files_final/ttf/'+fontName+'.ttf')
